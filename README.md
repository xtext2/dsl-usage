This example project uses 2 models of the same DSL in 2 different modules 
of a gradle multi-module project.

The model in the interactors module depends on the model in the domain module.

Links:

* The [DSL project](https://gitlab.com/tmtron/ex_xtext2_dsl)
* Related [xtext forum post](https://www.eclipse.org/forums/index.php?t=msg&th=1070741&goto=1779840&#msg_1779840)
